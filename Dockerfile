FROM nginx:latest
COPY Docker/nginx.conf /etc/nginx/nginx.conf 
COPY Docker/index.html /usr/share/nginx/html
EXPOSE 80
#test
CMD ["nginx", "-g", "daemon off;"]